<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');


Route::get('/test', 'TestController@index');


Route::get('/', ['middleware' => 'auth', function () { return view('home');  }]);

Route::get('/home', ['middleware' => 'auth', function () { return view('home');  }]);

Route::get('/story', ['middleware' => 'auth', function () {  return view('story'); }]);

Route::get('/wedding', ['middleware' => 'auth', function () {
    return view('wedding');
}]);

Route::get('/travel', ['middleware' => 'auth', function () {
    return view('travel');
}]);

Route::get('/accommodation', ['middleware' => 'auth', function () {  return view('accommodation');  }]);

Route::get('/gifts', ['middleware' => 'auth', function () {
    return view('gifts');
}]);

Route::get('/what-to-pack', ['middleware' => 'auth', function () {
    return view('pack');
}]);

Route::get('/local-tips', ['middleware' => 'auth', function () {
    return view('tips');
}]);

Route::get('/rsvp', ['middleware' => 'auth', function () {
    return view('rsvp');
}]);

Route::get('/activities', ['middleware' => 'auth', function () {
    return view('activities');
}]);

Route::get('/general', ['middleware' => 'auth', function () {
    return view('general');
}]);


@extends('layouts.auth')

@section('title', 'Ponta Do Ouro 2016')

@section('content')

    <div class="text-center">
        
        <p><img src="/img/chris-and-nola.png" height="50" width="220" /></p>

@if (count($errors) > 0)
<p>
        @foreach ($errors->all() as $error)
            {!! $error !!}
        @endforeach
</p>
@endif

        <form method="POST" action="/auth/login">
            {!! csrf_field() !!}

            <input type="hidden" name="email" value="guest@chrisandnola.com">

            <p>
                Password
                <input type="password" name="password" id="password">
            </p>

            <input type="hidden" name="remember" value="on">

            <p>
                <button type="submit">Enter</button>
            </p>
        </form>

    </div>

@stop


@extends('layouts.master')

@section('title', 'Ponta Do Ouro 2016 - Accommodation')

@section('content')

<div class="narrowed">

    <h1>Gifts</h1>
    <p>
        Your attendance at our wedding is the greatest gift that you could give to us.
        We are very grateful to all our loved ones who are making the effort to travel a long way 
        to be there for our big day.
    </p>
    <p>
        <br>However, if you would like to give us a gift above and beyond, we will be most thankful to
        accept a gift of spending money for our honeymoon.
    </p>
    <p>
        You can either give money in an envelope on our wedding day, or if you prefer
        you are welcome to make a bank deposit into our honeymoon savings account:
    </p>
    <br>
    <p class="text-center">
        Absa Savings<br>
        Account number: 621918850<br>
        Branch code: 632005<br>
        Reference: [Your name] 
    </p>
    <br>
    <img src="img/love-map.png" width="100%" />

</div>


@stop


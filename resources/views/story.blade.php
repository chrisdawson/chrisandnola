@extends('layouts.master')

@section('title', 'Ponta Do Ouro 2016 - Our Story')

@section('content')


<div class="narrowed">

    <h1>Chris says...</h1>

<img src="img/chris.png" width="200" align="right" />

    <p>

Our paths never quite crossed at high school. Although we were both there roaming the corridors,
going to that same assembly in the hall every week, it wasn't yet our time to meet.
</p><p>

I first encountered Nola on a warm summer evening in 2005 at the church. It was a relaxed setting where we ate dinner in
a group with other twenty-somethings and got slightly philosophical about life.  Her radiant smile and character
took me captive from our first conversation.  A sophisticated, bubbly and beautiful lady, intelligent and
(as the saying goes) full of beans!
I knew that we would become friends. 
</p><p>

We began to hang out together, sometimes in a group, other times just us.  I appreciated her openness, her
wisdom.  She knew about the world in a way that intrigued me.  Politics, history, theology and other topics
on which I didn't have much to add.  I enjoyed listening as she began to shape my understanding
of the world in a new way.
</p><p>

We enjoyed eating together on lunch break, going out for sushi or to one of the local restaurants in Bedfordview, where
we both worked.  I remember being very impressed one afternoon when she presented me with her home
made <em>paneer masala</em>.  We bonded in those early days over our appreciation of great cuisine
from all over the world, experiencing new flavours together. 
</p><p>

In December of 2011 we started falling in love.  We travelled together to a close friend's wedding in the
misty KwaZulu-Natal Midlands, and a powerful attraction had already begun to form.  We had our first kiss in
Hilton, and our romantic moment on the hotel terrace did not go unnoticed by our friends and family at the
wedding reception!
</p><p>

Since then our love for one another has grown and strengthened by leaps and bounds. 
I worked up the courage to ask Nola to marry me in Northern Ireland while we were on a holiday together visiting 
friends.
She wholeheartedly gave me
her reply: “Yes of course I’ll marry you!”
</p>

    </p>

<h1> Nola says...</h1>

<img src="img/nola.png" width="200" align="left" />

<p>
I remember the first time I met Chris. It was at a course at our church. He greeted me warmly and I can
recall noticing his kind blue eyes and gentle mannerisms. By the end of that evening I knew I wanted to be his
friend. Chris had a way about him, I can’t really put it to words but he got my attention. He quickly became
one of my favourite people to hang out with. And we did a lot of that. We went to dinners together and
movies and parties and lunches &mdash; and sometimes dinner, a movie and then to a party all in one night.
Our friendship blossomed.  
</p><p>
I think back to a time when Chris played a song for me on his guitar and sang it too. I was lost for words.
My good friend, Nhlalala, who was standing beside me at the time didn’t miss the opportunity to tease me
about Chris liking me. I wasn’t convinced though. 
</p><p>
Chris and I always seemed to be going down different paths during our seven years of friendship. However our paths joined at the end of 2011, when we had been spending a lot of time together again. In December of that year I remember sharing with a friend that I had fallen for Christopher David Dawson. 
</p><p>
Chris and I expressed our feelings for each other at a friend’s wedding in Hilton. This man who had been a great friend to me for so many years had managed to win my heart! 
</p><p>
On the evening of the 30th December 2011 Chris asked me to be his girlfriend. In a typically cool and relaxed Chris way he asked me if I was free on this day every year. When I asked why, he casually said that it would be a great going-out date. So smooth. I said yes. He swept me off my feet (not literally!) and the great adventure of my life began that evening. 
</p></p>
A beautiful friendship turned into an amazing romance. On the 21st May 2014 Chris asked me to spend the rest of my life with him. I said Yes. Who could resist spending the rest of their life with their best friend?
</p>

 <br>
<div class="text-center">
	<img src="img/pics/01.jpg" width="250" class="polaroid" />
	<img src="img/pics/03.jpg" width="250" class="polaroid" />
	<img src="img/pics/02.jpg" width="250" class="polaroid" />
	<img src="img/pics/04.jpg" width="250" class="polaroid" />
	<img src="img/pics/05.jpg" width="250" class="polaroid" />
	<img src="img/pics/06.jpg" width="250" class="polaroid" />
	<img src="img/pics/07.jpg" width="250" class="polaroid" />
	<img src="img/pics/08.jpg" width="250" class="polaroid" />
</div>

</div>
    

@stop


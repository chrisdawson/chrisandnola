@extends('layouts.master')

@section('title', 'Ponta Do Ouro 2016 - General Info')

@section('content')

<div class="narrowed">
    <h1>General Info - Q&amp;A</h1>

    <p>We will list Frequently Asked Questions here &mdash; check back from time to time in case we have added more information.</p>

    <h3>How long should we stay for?</h3>

    <p>
        The trip to Ponta is quite a distance and to maximise your enjoyment, if you are able to take leave, we would 
        suggest at least a 3-4 night stay.   Consider arriving on Thursday 18 August and returning home on Monday 22 August (4 nights).
        If budget and working commitments allow, extend your holiday a few extra days and relax in Mozambique for a while.
    </p>

    <h3>Can we use a normal car to get to Ponta?</h3>

    <p>
        Yes.  You can use your car to get to the Kosi Bay border, use the overnight parking
        and get picked up by a shuttle which will take you to Ponta.
        You will only be able to take 
        your car across the border if you have a 4x4 vehicle.
        The shuttle option is very convenient and will be the best for most of
        our guests to use.  Please find more info on our <a href="travel">travel page</a>.
    </p>

    <h3>Will I need a Passport or Visa to get into Mozambique?</h3>

    <p>
        It might be obvious but we should first mention that you will definitely be needing a valid passport!
        Passports should be valid for at least 6 months after your visit ends.
    </p>
    <p>
        South African residents luckily don't need a Visa to visit Mozambique. Score!<br>
        For our guests travelling from the U.K. or U.S.A,  you will need a Visa.  We will contact you with all the information regarding the Visa and entry requirements.
    </p>

    <h3>What time does the border close?</h3>
    <p>
        The Kosi Bay border post is open between 08:00 and 17:00 every day.
        The border crossing process does not take too long, perhaps half an hour.
    </p>

    <h3>Is there anything I should not bring across the border into Mozambique?</h3>

    <p>
        Yes &mdash; especially beer.  See our <a href="what-to-pack">what to pack</a> page for more info on what to and what not to bring.
    </p>

    <h3>What will the weather be like?</h3>

    <p>
        Temperatures will be in the region of 22-30 &deg;C.
        There should be a bit of a breeze, especially in the evening as it gets a bit cooler.
        Ponta has a tropical, humid climate.
        It should not be too rainy at this time of year but be prepared for light rain just in case.
    </p>

    <h3 id="dresscode">What is the dress code for the wedding?</h3>

    <p>
        Get dressed up in your smart beach attire.  We would love to see pale (or earthy, neutral) colours.
        Maybe a hat as well!  No denims please.<br>
        <b>Ladies</b>: Cocktail dress or slacks and top, but no high heels.
        You could find some ideas on <a target="_blank" href="https://www.google.co.za/search?q=beach+cocktail+attire" >Google Images</a>. <br>
        <b>Gents</b>: Light colours, and stay away from that hawaiian shirt!
    </p>
    <p>Be prepared for it to cool down a bit later in the evening.
        You can bring some warmer clothing with you to the beach, which will be taken to the
        reception venue for you
        ahead of time.
    </p>

    <h3>Do I need to take anti-malaria medication?</h3>

    <p>
        The whole of Mozambique is <em>officially</em> a malaria risk area.
        
        However, common advice is that there is a close-to-zero risk of contracting malaria in Ponta Do Ouro.
        When we visit Ponta we personally don't take anti-malaria drugs.  
        (Do bring mosquito repellants though).

        You may decide to use medication to put your mind at ease, in which case you should visit a travel clinic in your area ahead of time to obtain the meds.
    </p>

    <h3>What kind of money is accepted in Ponta Do Ouro?</h3>
    <p>
        The Mozambiquan currency is the Metical (one Rand equivalent to about 4.5 Mets), but South African Rand is accepted
        everywhere in Ponta so there is no need to exchange money.
        If you pay for items in Rand you may receive change in either Rand or Meticais.
    </p>
    <p>
        You can also use most kinds of credit and debit cards at the local shops and restaurants.
    </p>

    <h3>How do we share photos?</h3>
    <p>
        Our official hashtag is #ThatPontaWedding so go crazy on Instagram, Facebook or Twitter.
        We'd love to experience Ponta through your eyes.
        Connectivity on that side might be an issue, which leads us to...
    </p>

    <h3>How's the cellphone service in Mozambique?</h3>
    <p>
        There is cell signal in Ponta, enough to make calls and send SMSs.
        It would be useful to have at least one phone per family with roaming enabled so that you can stay in contact
        with others, or phone the <a href="/travel">shuttle service</a> when it's time to head back home.
        You can enable roaming on the road, about half an hour before you reach the border.
        Watch out for heavy data charges - it might be better to keep cellular data turned off while in Mozambique, if you can!
    </p>


</div>


@stop


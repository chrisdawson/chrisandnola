@extends('layouts.master')

@section('title', 'Ponta Do Ouro 2016 - Activities')

@section('content')


<div class="narrowed">

    <h1>Activities</h1>

    <p>There is so much to see and do in Ponta.  Plan your stay in such a way that you'll get to see
        things that interest you. For the adventurous, you can do some of the activities such as scuba diving,
        snorkelling and swimming with dolphins. </p>


    <h3>Dolphin encounters &amp; scuba diving</h3>
    <p> 
        Nola says: Do swim with the dolphins. It's amazing! Book with Angie from Dolphin Encountours on +258&nbsp;84&nbsp;330&nbsp;3859; she is friendly and helpful, and will 
        be able to offer an unforgettable experience.
</p><p>For those into scuba diving, Ponta is a great place to go.  
        There are lots of tour operators offering Scuba diving, so do some of your own
        research and compare reviews before making a booking.
    </p>

    <img src="img/ponta/back-to-basics.jpg" width="200" align="right" style="margin-left: 20px; margin-bottom: 20px;">

    <h3>Quad biking</h3>

    <p>Best place to book a quad bike is Back to Basics at ± R500 per day.
        Chat with them about hourly or half day rates.
        Another option is "Biker Boys" across the road but they are more expensive.</p>

    <h3>Fishing</h3>
    <p> 
        For a deep sea fishing expedition, book with Nirvana Fishing Charters or Gozo Azul.
        Check local regulations if you plan to bring your catch home with you across the border.
    </p>


    <h3>Restaurant &amp; bar hopping</h3>
    <p>We recommend stopping in at Fernando's bar and drinking the R&amp;R (rum and raspberry), a brightly
        coloured local favourite cocktail.  It's one of Ponta's trademarks!
        Pinto's Beach Bar and Florestina's along the beachfront are also worth visiting.
    </p>

    <h2>Weekend plans</h2>

    <p>Here are some other informal plans we hope you'll join in on.
        Let us know via the <a href="rsvp">RSVP form</a> if you will be able to make it.</p>

    <h3>Thursday, 18 August 2016</h3>
    <p>Cocktails / smoothies at Mango Caf&eacute; - a great spot above the Dolphin Center on the main road in Ponta.</p>
    <p>Time: 4:00 PM until 7:00 PM</p>

    <h3>Friday, 19 August 2016</h3>
    <p>We are having a pre-wedding dinner at <a target="_blank" href="http://www.pontadoouro.co.za/baleiavista/">Baleia &agrave; Vista</a>
        and we really hope you will be able to join us!
    </p>
    <p>Time: 6:00 PM until 9:00 PM<br>
        Cost:  Average price of a main course is around R120.<br>Full meal with starter, main and dessert: around R250 per person.</p>

        <p>Please keep in mind that Baleia &agrave; Vista is about 30 minutes walk from Ponta's main beach.
            It is a beautiful spot with a deck overlooking the sea.</p>
</div>


@stop


@extends('layouts.master')

@section('title', 'Ponta Do Ouro 2016 - Local Tips')

@section('content')

<div class="narrowed">
    <h1>Local tips</h1>

    <h2>Things to do and places to go</h2>

    <p>
        You will spend some time at some of our favourite restaurants in Ponta, including Tarragon, Love Caf&eacute;, Baleia Vista and of course the Beach Bar (our wedding venue).
        For the adventurous, you can do some of the activities such as scuba diving, snorkelling and
        swimming with dolphins.  For more details visit our <a href="activities">activities page</a>.
    </p>

    <h3>Mango Caf&eacute;</h3>
    <p>Above the Dolphin Centre you will find the Mango Caf&eacute; serving smoothies, cocktails and a breakfast/lunch menu.
        Come and join us here at 4:00&nbsp;PM on Thursday, 18 August if you're able to be in Ponta by that time already.</p>


    <h3>Fernando's</h3>
    <img src="img/ponta/fernandos.jpg" width="200" align="right" style="margin-left: 20px; margin-bottom: 20px;">

    <p>We recommend stopping in at Fernando's bar and trying the R&amp;R (rum and raspberry), a
        local favourite cocktail.  It's signature Ponta!
        Bring a plastic card to fix to the bar counter, if you would like to leave your mark at this famous watering hole.
    </p>

    <h3>Pinto's Beach Bar</h3>
    <p>A great place to relax and have a drink, there is usually a DJ playing some awesome music.</p>

    <h3>Tarragon Caf&eacute;</h3>
    <p>Drop by for coffee and cake.</p>

    <h3>Love Caf&eacute;</h3>
    <p>Grab a slice of cake or some freshly baked bread.</p>



    <h3>Fala Portugu&ecirc;s</h3>
    <p>
        Brush up on some basic Portuguese before you arrive, and this will assist communicating with the local people.
        Most in the tourism industry do speak some English, but it's always good to know a few key phrases.</p>

        <li>Good morning: <b>Bom dia</b>
        <li>Good afternoon: <b>Boa tarde</b>
        <li>Good evening: <b>Boa noite</b>
        <li>How are you?: <b>Como est&aacute;?</b>
        <li>I'm well: <b>Estou bem</b>
        <li>Thank you: <b>Obrigado</b> (if you are male) / <b>Obrigada</b> (if you are female)
    </p>

    <p><em>"If you talk to a man in a language he understands,
        that goes to his head. If you talk to him in his language, that goes to his heart."</em> &mdash; Nelson Mandela</p>

    <h2>Don'ts</h2>
    <li>DON'T take photographs of government officials and government buildings (police station etc.)</li>
    <li>DON'T drive your 4x4 on the beach
    <li>DON'T fish on the beach without a permit
    <li>DON'T make a fire on the beach
    <li>DON'T sign up for jet-skiing &mdash; it scares the dolphins


</div>


@stop


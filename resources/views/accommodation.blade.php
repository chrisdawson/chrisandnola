@extends('layouts.master')

@section('title', 'Ponta Do Ouro 2016 - Accommodation')

@section('content')

<div class="narrowed">
    <h1>Accommodation</h1>
    <p>
        The beautiful town of Ponta d' Ouro offers a wide range of overnight options.
        We would like to recommend some great places (all within walking distance to our wedding venue).
    </p>

    <h2>Coco Rico</h2>

<img src="img/accom/cocorico.jpg" width="200" align="right" style="margin-left: 20px; margin-bottom: 20px;">

    <p>

Our top recommendation.  Very near to our wedding venue.  8 sleeper units (but with 3 bedrooms &amp; 3 bathrooms).
We can organise and match you up with other guests if you'd like to share.
Just let us know, on our <a href="/rsvp">RSVP form</a>.
<br><br>
Air conditioned: Yes<br>
Distance from venue:  5-10 minute walk<br>
Extras:  Swimming pool, restaurant<br>
Pricing:  R2200 per unit per night - 10% discount if you mention you are part of our wedding.<br>
<br>
Link:  <a href="http://www.cocorico.co.mz/accommodation" target="_blank">http://www.cocorico.co.mz/accommodation</a><br>
Call: Tercia 034-413-1418<BR>
Email:  <a href="info@cocorico.co.za">info@cocorico.co.za</a>

    </p>

<h2>Mar e Sol</h2>

<img src="img/accom/maresol.jpg" width="200" align="right" style="margin-left: 20px; margin-bottom: 20px;">


<p>
    Lovely 8 sleeper units (4 bedrooms, each with en suite bathroom).  Situated up on the hill near to the main beach
    where our wedding is taking place.  Book early as availability is limited.
<br><br>
Air conditioned:  Yes<br>
Distance from venue:  5-10 minute walk<br>
Extras:   Beautiful sea views from patio deck<br>
Pricing:  R340 per person per night - minimum of 4 people<br>

<br>
Link:  <a href="http://www.maresol.co.za/accommodation.php" target="_blank">http://www.maresol.co.za/accommodation.php</a>
</p>


    <h2>Ntsuty Lodge</h2>

    <img src="img/accom/ntsuty.jpg" width="200" align="right" style="margin-left: 20px; margin-bottom: 20px;">

    <p>
        A local word meaning "shade", Ntsuty is (as you would expect) very shady.
A short walk from the beachfront.  We have stayed there twice before and enjoyed our time there.
Range of units from 4 up to 8 sleepers.
Note:  4 sleeper units accommodate 2 adults comfortably.
<br><br>
Air conditioned:  Only in lounge<br>
Distance from venue:  5-10 minute walk<br>
Extras:  Swimming pool, mosquito nets<br>
Pricing:  R380 - R475 per person per night<br>
<br>
Link: <a href="http://www.ntsutylodge.com/accommodation.html" target="_blank">http://www.ntsutylodge.com/accommodation.html</a>

    </p>

    <h2>Ocean View Cabanas</h2>

    <img src="img/accom/oceanview.jpg" width="200" align="right" style="margin-left: 20px; margin-bottom: 20px;">

    <p>
As you would expect this place offers an ocean view :)<br>
We have stayed here before and it was a pleasant stay.  More suited to a family with kids.
There are 4 chalets available, sleeping between 4 and 8 people.
<br><br>
Air conditioned:   Yes<br>
Distance from venue:  5-10 minute walk<br>
Pricing:  From R800 per unit per night (4 sleeper)<br>
<br>

Link:  <a href="http://www.oceanview4.com/what-to-expect/" target="_blank">http://www.oceanview4.com/what-to-expect/</a>

    </p>

<h2>The Whaler</h2>
<img src="img/accom/whaler.jpg" width="200" align="right" style="margin-left: 20px; margin-bottom: 20px;">

<p>Recently renovated smaller cabins sleeping 2-3 people, opening straight onto the beach.
We wouldn’t recommend the reed rooms unless you are keen to rough it!
<br><br>
Air conditioned:  No<br>
Distance from venue:  &lt; 5 minute walk<br>
Extras:  Mosquito nets<br>
Pricing:  R230 - R600 per person per night<br>
<br>
Link:  <a href="http://www.thewhaler.co.za/The%20Whaler%20Accommodation.html" target="_blank">http://www.thewhaler.co.za/The%20Whaler%20Accommodation.html</a>
</p>



    <h2>Ponta Beach Camps / Scuba Adventures</h2>
    <img src="img/accom/beachcamp.jpg" width="200" align="right" style="margin-left: 20px; margin-bottom: 20px;">

    <p>
Pick from beach front cabins with private en suite bathroom, or safari tents if you want a cheaper low key option.  
Communal kitchen and bathroom facilities available.  Restaurant and bar on premises.
<br><br>
Air conditioned:  No<br>
Distance from venue:  &lt; 5 minute walk<br>
Extras:  “Backpacker” vibe<br>
Beach Front Unit: R540 per unit per night (2 sleeper)<br>
Deluxe Safari Tent:  R200 per person per night (2 sleeper)<br>
<br>
Link:  <a href="http://www.pontabeachcamps.co.za/accommodation-options/accommodation.html" target="_blank">http://www.pontabeachcamps.co.za/accommodation-options/accommodation.html</a>

    </p>

    <h2>Baleia &agrave; Vista</h2>
    <img src="img/accom/baleia.jpg" width="200" align="right" style="margin-left: 20px; margin-bottom: 20px;">

    <p>
A resort situated on the Southern side of Ponta, a bit further from our venue.  It offers
luxury camping in permanent tents with en suite facilities.   You should only consider staying here if
you are bringing your own vehicle or are prepared to walk a fair distance.
<br>
The restaurant at Baleia Vista is the venue for our pre-wedding dinner on 19 August &mdash; we hope you'll arrive in time to join us!
<br><br>
Air conditioned:  No<br>
Distance from venue: 15-20 minute walk<br>
Extras:  Restaurant, bar and swimming pool<br>
Pricing:  R720 - R990 per luxury tent per night (sleeps 2-4)<br>
<br>
<!-- Link:  <a href="http://www.baleiavista.com/" target="_blank">http://www.baleiavista.com/</a -->
Link:  <a href="http://www.pontadoouro.co.za/baleiavista/" target="_blank">http://www.pontadoouro.co.za/baleiavista/</a>


    </p>
    

    <h2>Not recommended</h2>
    <p>
        There are certain places in Ponta Do Ouro which might seem convenient or good value, however we would advise to steer clear.
        <ul>
            <li>Paraiso Do Ouro - rooms are not well-maintained</li>
            <li>"Barracas" at Ponta Beach Camps - these are reed huts, not the greatest.
                Rather go for their beach front units or safari tents.</li>
        </ul>
    </p>

@stop


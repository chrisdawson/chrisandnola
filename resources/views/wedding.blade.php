@extends('layouts.master')

@section('title', 'Ponta Do Ouro 2016 - Wedding')

@section('content')


<div class="narrowed">

    <div class="jumbotron text-center" style="margin-top: 30px;">
        <p style="line-height: 45px; width: 60%; margin: 0 auto;">Please join us as we celebrate
        the beginning of our new life together
        on Saturday, the 20th of August 2016</p>
        
        <p><br><img src="/img/divider-small.png" width="300"></p>


        <h2>The Ceremony</h2>
        <p>Praia (beach)<br>Near Motel Do Mar<br>3:00 PM</p>
        <h2>The Reception</h2>
        <p>Ponta Beach Bar<br>6:00 PM</p>

        <p><img src="/img/divider-small.png" width="300"></p>


        <h3>Dress code</h3>
        <p>Smart beach attire<br>
            <span style="font-size: 14px"><a href="/general#dresscode">more info</a></span>
            </p>
    </div>


    <div class="text-center">
        <a target="_blank" style="font-size: 10px;"
        href="http://www.freepik.com/free-vector/sketchy-text-dividers_811570.htm">Vector design by Freepik</a>
    </div>

</div>


@stop


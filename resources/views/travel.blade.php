@extends('layouts.master')

@section('title', 'Ponta Do Ouro 2016 - Travel')

@section('content')


    <div class="narrowed">

    <h1>Travel</h1>


    <p>We've done the trip from Jo'burg to Ponta a number of times, so we have some advice to share.</p>

    <p>The biggest part of the adventure will be getting yourself to the
        <a href="https://www.google.co.za/maps/place/Kosi+Bay+Border+Control+(RSA)+08h00+-+17h00/@-26.8669518,32.7887598,12.76z/data=!4m2!3m1!1s0x1ee491e0f6a3ffcf:0xa48330c4bfebebda?hl=en" target="_blank">Kosi Bay border gate</a>.
        This can be done using any vehicle.
        Important to note: the border gate closes at 17:00, so it's best to plan to arrive around 15:00 or earlier, in
        case of delays on the road.</p>
    <p>Once you cross the border into Mozambique, there is a further 30 minute trip to Ponta over a very rough
        and bumpy sand road (12km over the dunes).  This is only for 4x4 vehicles and is not possible with
        a regular street car :)
        </p>
    <h3>Parking and shuttle service</h3>
    <p>Fortunately, even if you don't have a 4x4, it's very easy to get to Ponta.  As you approach the
        border, on the left there is a parking area where you can book your car in for a few days.  It costs
        R40 per day and there is 24 hour security.  You will find your car there safe and sound when
        you return.</p>

    <BR>
    <img src="img/ponta/parking.jpg" width="100%" /><br>
    <div class="text-center" style="color: #888"><em>Kosi Bay border parking &mdash; R40 per day</em></div>
</p><br>

    <img src="img/ponta/shuttle.jpg" width="200" align="right" style="margin-left: 20px; margin-bottom: 20px;">


    <p>You will need to organise with one of the local Ponta shuttle services to pick you up from the border and take
        you to your <a href="accommodation">accommodation</a>.  This is best done over the phone 1-2 days before
        your arrival.  Please contact one of the following drivers, who will be happy to assist:</p>
    <ul><li>Sandra: +258 84 731 9049 (available on Whatsapp)</li><li>Isaac: +258 84 200 3717</li><li>Beto: +258 84 507 7822</li></ul>

    <p>The shuttle drivers charge ± R200 per person (return) for transport between Ponta and the border.  They also 
         offer discounts for a group of 6 or more people together.</p>



            <h2>Driving from Johannesburg</h2>

<p><br>
            <a href="https://www.google.com/maps/dir/Greater+Johannesburg+Metropolitan+Area,+South+Africa/Ponta+do+Ouro,+Mozambique/@-26.902475,30.622171,7z/data=!4m14!4m13!1m5!1m1!1s0x1e950966aff4e0fb:0x2dc747f800e591b0!2m2!1d27.9717606!2d-26.1704415!1m5!1m1!1s0x1ee48fe4aa142291:0x16ccd65ad81faec0!2m2!1d32.8859972!2d-26.8427388!3e0?hl=en-US"
            target="_blank"><img src="/img/map.png" width="100%" /></a>
            <br>
            <div class="text-center" style="color: #888"><em>The recommended route from Jo'burg</em></div>
</p><br>

            <p>Please make use of the N17 / N2 route.  It's the one highlighted in
                blue on the map above. You'll pass through Bethel, Ermelo, Piet Retief and Pongola.</p>

            <p><strong>Important to note!</strong> There is usually some construction work happening on this
                road, not to mention trucks which can be difficult to overtake.  Please be prepared for a
                <strong>9-10 hour journey</strong>, and take it safe with a few short rest stops along the way.</p>

            <p>Our advice is to depart Jo'burg no later than 6:00 AM on the day of travel.  If you would like to 
                do more than two rest stops, it's better to leave even earlier.
                When we did the trip in August 2015, we stopped off for breakfast at the <a href="http://www.destinationsmp.co.za/listing/wimpy-bethal.html"
                target="_blank">Wimpy in Bethal</a>, 
                and for lunch at the <a href="http://www.fishaways.co.za/restaurants.php?id=179" target="_blank">Fishaways /
                Steers in Pongola</a>.  Both were really good!
            </p>
            <p>Look out for livestock and school kids crossing the road, and overtake safely following the road markings.
                There are also some speed bumps to watch out for on the stretch of road between Jozini and Kosi Bay.
                Otherwise, the drive is enjoyable and quite scenic, especially around Jozini Dam.


            <h2>Travelling from other places</h2>


            <h3>Durban</h3>
            <p>Driving to Ponta from Durban is quite straightforward and should take about 6-7 hours including stops.
                Keep the sea on your right hand side and you should be OK!</p>

            <h3>Far far away</h3>
            <p>If you're coming to our wedding from abroad, or from the Eastern / Western Cape, the best option
                will be to fly to the Johannesburg, Durban, or Richards Bay airport, rent a car and travel by road to Ponta.
                Flying into Maputo airport is not a good option &mdash; the road between Maputo and
                Ponta is ridiculously bad and the (very bumpy) drive will take more than 6 hours in a 4x4.
            </p>


            <h2>Bringing your own vehicle into Mozambique</h2>

            <p>
                If you have a vehicle with 4x4 capability and decent amount of ground clearance, you can bring 
                it across the border instead of making use of the parking and shuttle service.
                There are a few things to keep in mind.
            </p>
            <ul>
                <li>Bring the vehicle registration papers along</li>
                <li>You will need to pay third party insurance and a temporary
                    import duty (somewhere between R150-R220 in cash) at the border.
        
            <li>Deflate your tyres at the border to around 120 kPa for driving on sandy roads.
                You will be able to re-inflate at the nearest garage after crossing the border back into SA.</li>

                <li>If you don't have 4-wheel drive vehicle, you could use a 2x4 / diff lock vehicle
                    with high ground clearance.
                However you will run the risk of getting stuck in the sand and might need help getting towed out, so only
                 experienced off-roaders should try this option.</li>
            </ul>


        <p>&nbsp;</p>

        <p>
            Related info:
            <ul>
                <li><a href="what-to-pack">What to pack</a></li>
                <li><a href="local-tips">Local tips</a></li>
            </ul>
        </p>

    </div>


@stop


@extends('layouts.master')

@section('title', 'Ponta Do Ouro 2016 - What to Pack')

@section('content')

<div class="narrowed">

    <h1>What to pack</h1>

    <p>Everything you'd normally bring for a holiday at the coast!
    	Here are a few extra notes on what you might want to bring with you.</p>

    <h3>Clothing</h3>
    <p>We are expecting mild weather in August, however it might get a bit hot, chilly or windy.  Pack for summer but with a few additional layers in case the weather turns.
    	Some comfortable shoes are a must, as the main mode of transport in Ponta is walking &mdash; mainly on soft sand.
        Flip flops are a great idea... high heels not so much!</p>
        <p>For the wedding day, our dress code is "beach smart, light colours".  For gents, 
            please no denims and
            no hawaiian shirts!  For ladies, you could wear a dress (e.g. cocktail dress) or loose slacks and a top.
            Again, high heels not a good idea.</p>

    <h3>Food &amp; drinks</h3>
    <p>
        There are many excellent bars, restaurants and eating places around. 
        If you've chosen to stay in <a href="accommodation">accommodation</a> with kitchen and/or braai
        facilities, you could do some of your own meals.
        There are limited grocery shopping and butchery options available, so bring food in a cooler bag if
        planning to self-cater.
        <!-- Last stop along the road if you've forgotten to pick up anything is the Spar at
        Manguzi (KwaNgwanase). --></p>
    <p>
        Bring about 2 litres of drinking water per person per day, or buy from the shops or restaurants
        while you are there.  It's better to avoid tap water and stick to drinking bottled water - or something stronger!
    </p>


    <h3>Medication</h3>
    <p>
        Pack a first aid kit and bring all your personal medication.
        Keep in mind that there is no hospital or resident doctor in Ponta Do Ouro.
        There is however a pharmacy in town catering for a few basic needs.
    </p>
    <p>
        Expect to meet a few mosquitos, so bring some protection.
        e.g. Long pants, mosquito repellant sprays, citronella candles.
        Also, definitely pack a bottle of sunscreen.
    </p>

    <h3>Don't bring</h3>

    <p>
        There is a restriction against importing beer into the country from South Africa.
        Mozambiquan local beer is very good and worth trying!  Our favourites are the very popular
        2M (pronounced dosh-em) and Laurentina.
        You can however bring other alcohol into the country such as wine and spirits in limited quantities:<br>
        Wine:  2.25&#8467; (3 bottles) per person duty free<br>
        Spirits:  1&#8467; per person
    </p>
</div>

@stop


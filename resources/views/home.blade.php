@extends('layouts.master')

@section('title', 'Ponta Do Ouro 2016')

@section('content')

    <div class="jumbotron text-center main-jumbo">
        <h1>We're getting married!</h1>
        <h2>Ponta Do Ouro, Mozambique</h2>
        <h2>20th August 2016<br>3:00 PM</h2>
    </div>

    <div class="row">

        <div class="col-md-4 home-promo">
            <img src="img/chris-nola-ireland.jpg" style="width: 100%" />
            <h2>Our Story</h2>
            <p>Starting off with a great, long friendship and culminating on 20 August, our wedding day.
                This is only the beginning of our adventure...</p>
            <p>
                <a class="btn btn-lg btn-primary" href="story" role="button">Read our story &raquo;</a>
            </p>
        </div>

        <div class="col-md-4 home-promo">
            <img src="img/beach-camp.jpg" style="width: 100%" />
            <h2>Venue</h2>

            <p>Everything you need to know about getting to our venue in magical
                Mozambique &mdash; <a href="travel">travel</a>,
                <a href="accommodation">accommodation</a> and all the other <a href="general">info you'll need</a>.</p>
            <p>
                <a class="btn btn-lg btn-primary" href="general" role="button">About Ponta &raquo;</a>
            </p>
        </div>

        <div class="col-md-4 home-promo">
            <img src="img/date-sand.jpg" style="width: 100%" />
            <h2>RSVP</h2>
            <p>We really hope you can be there on our special day!
                Please let us know before <strong style="white-space: nowrap">1 July 2016</strong> whether
                you will be able to make it.</p>
            <p>
                <a class="btn btn-lg btn-primary" href="rsvp" role="button">Send RSVP &raquo;</a>
            </p>
        </div>

    </div>

@stop

